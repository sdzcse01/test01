package com.service.gfgdf.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-05-21T08:41:38.211Z")

@RestSchema(schemaId = "gfgdf")
@RequestMapping(path = "/gfgdf", produces = MediaType.APPLICATION_JSON)
public class GfgdfImpl {

    @Autowired
    private GfgdfDelegate userGfgdfDelegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userGfgdfDelegate.helloworld(name);
    }

}
